
# 前端技术学习资源清单

前端技术学习资源清单，不定期更新。

清单覆盖范围：公开的 HTML、CSS、JavaScript 文档库；主要的开发工具、语言（如扩展语言、流程控制工具等）网站、下载地址和教程；主要的 MVVM 框架和 Node 开发框架的官方文档；辅助工具（如网络代理、源代码编辑器）相关资源；设计素材资源网站；测试框架和技术文档相关内容。

由于个人原因，对 Ang/VUE 没有深入研究，希望得到帮助；另外，需要补充前端 3D 场景渲染相关的内容，希望得到帮助。 

欢迎提交 Issues 或者发起 PR 补充内容，收录内容建议以中文为主。

---

## 0.预备

### 0.1.编辑器

* Visual Studio Code [链接](https://code.visualstudio.com/)

### 0.2.网络代理

* v2ray 用户手册 [链接](https://www.v2ray.com/)
* v2ray-Core Releases [链接](https://github.com/v2ray/v2ray-core/releases)

### 0.3.Git 

* Git-Download [链接](https://git-scm.com/downloads)

* Git 简易指南 [链接](http://www.bootcss.com/p/git-guide/)
* 常用 Git 命令清单-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2015/12/git-cheat-sheet.html)
* Git 教程-廖雪峰的官方网站 [链接](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)

---

## 1.HTML+CSS

### 1.1.文档库

**HTML**
* HTML-MDN [链接](https://developer.mozilla.org/zh-CN/docs/Web/HTML)
* HTML-W3School [链接](http://w3school.com.cn/html/)

**CSS**
* CSS-MDN [链接](https://developer.mozilla.org/zh-CN/docs/Web/CSS)
* CSS-W3School [链接](http://w3school.com.cn/css/)

### 1.2.CSS预处理

**LESS**
* LESS，基于 NodeJS 的 CSS 预处理工具，官方网站 [链接](http://lesscss.org/)
* LESS 中文文档，[链接](https://less.bootcss.com/)

**SCSS**
* SASS，基于 Ruby 的 CSS 预处理工具，官方网站 [链接](http://sass-lang.com/)
* SASS 中文文档，[链接](http://sass.bootcss.com/)
* Ruby-Download [链接](https://www.ruby-lang.org/zh_cn/downloads/)

### 1.3.其他

* FreeCodeCamp [链接](https://www.freecodecamp.cn/)
* normalize.css 源码(7.0.0) [链接](https://github.com/necolas/normalize.css) 

---

## 2.JavaScript

**推荐阅读**

* 编程风格-ECMAScript 6 (阮一峰/ECMAScript 6 入门) [链接](http://es6.ruanyifeng.com/#docs/style)

### 2.1.核心

* JavaScript-W3School [链接](http://w3school.com.cn/js/)
* JavaScript-MDN [链接](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript)
* 重新介绍 JavaScript（教程）-MDN [链接](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/A_re-introduction_to_JavaScript)
* JavaScript 全栈教程（基础）-廖雪峰的官方网站 [链接](https://www.liaoxuefeng.com/wiki/001434446689867b27157e896e74d51a89c25cc8b43bdb3000)
* 《ECMAScript 6 入门（第三版）》-阮一峰著 [链接](http://es6.ruanyifeng.com/)

### 2.2.开发框架

*Angular*
* Angular，由 Google 发起并维护的知名 MVVM 模型设计框架 [链接](https://angular.cn/docs)
* Angular 中文文档(v5.0.0.0 beta.4) [链接](https://angular.cn/docs)
* AngularJS(v1) 教程-菜鸟教程 [链接](http://www.runoob.com/angularjs/angularjs-tutorial.html)

*VUE*

> 本部分内容待完善

* VUE，前端 MVVM 模型设计框架 [链接](https://vuejs.bootcss.com/)

### 2.3.React 技术栈

* React，由 Facebook 设计的用户界面设计库，官方网站 [链接](https://reactjs.org/)
* React 中文网站 [链接](https://www.reactjscn.com/)
* JSX 语法-如也，[链接](http://seanhuai.github.io/2017/11/react-jsx)

*React*
* React 入门实例教程-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2015/03/react.html)
* React 样例库-Github [链接](https://github.com/ruanyf/react-demos)
* React 脚手架项目库-Github [链接](https://github.com/ruanyf/react-babel-webpack-boilerplate)

*React-Router*
* React-Router 官方库-Github [链接](https://github.com/ReactTraining/react-router)
* React-Router 2.x 使用教程-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2016/05/react_router.html)
* React-Router 4.x 完全指南 [链接](http://www.zcfy.cc/article/react-router-v4-the-complete-guide-mdash-sitepoint-4448.html)
* React-Router 4.x 样例库-Github [链接](https://github.com/reactjs/react-router-tutorial)

*React-Redux*
* Redux 架构中文文档 [链接](http://cn.redux.js.org/)
* Redux 入门教程-阮一峰的网络日志 (1)基本用法 [链接](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_one_basic_usages.html) (2)异步与中间件 [链接](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_two_async_operations.html)
* React-Redux 使用方法-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2016/09/redux_tutorial_part_three_react-redux.html)

*React 组件库*

> 本部分内容待扩充

* Ant Design Pro，使用 Ant Design 的中台前端、设计解决方案，[链接](https://pro.ant.design/index-cn)
* Ant Design Mobile，使用 Ant Design 的移动端组件库，[链接](https://mobile.ant.design/index-cn)
* Material-UI，基于 Google Materialize 设计的组件库，[链接](https://github.com/mui-org/material-ui)
* Amazu-UI-React，基于 AmazuUI 类库的 React 组件库，[链接](http://amazeui.org/react/)

*React Native*

> 本部分内容待完善

* React Native，基于 JavaScript 和 React 构建原生应用程序的框架，官方网站 [链接](http://facebook.github.io/react-native/)
* RN [链接](http://reactnative.com/)

### 2.4.TypeScript

* TypeScript，由 Microsoft 发起并维护的，基于 ECMAScript 的增强类型语言，[链接](https://www.tslang.cn/)
* TypeScript 中文文档，[链接](https://www.tslang.cn/docs/home.html)

### 2.5.开发工具

**Gulp**
* Gulp，前端工程自动化流开发工具，[链接](http://www.gulpjs.com.cn/)
* Getting Started with Gulp-如也，[链接](https://seanhuai.github.io/2017/07/gulp-getstarted/)

**模块化工具**
* Webpack，模块化资源打包工具，[链接](https://webpack.bootcss.com/)
* Webpack 教程（Demos）-阮一峰的网络日志 [链接](https://github.com/ruanyf/webpack-demos)

**JQuery**
* JQuery-W3school，[链接](http://www.w3school.com.cn/jquery/)
* JQuery API 中文文档，[链接](http://www.jquery123.com/)
* JQuery 插件库，[链接](http://www.jq22.com/)

**模板语言**
* Handlebars.js 模板设计引擎，官方网站 [链接](http://handlebarsjs.com/)
* Jade 模板设计引擎，官方网站 [链接](http://jade-lang.com/)
* EmbeddedJS 模板设计语言，官方网站 [链接](http://www.embeddedjs.com/)
* Markdown 模板语言，语法说明，[链接](http://wowubuntu.com/markdown/index.html)

**跨平台**
* WinJS，由微软提供的，用于设计 UWP 应用程序的 Windows 开发库，[链接](http://try.buildwinjs.com/)
* Electron，基于 Javascript 的跨平台应用生成工具，[链接](https://electron.atom.io/)

**设计类库**
* Bootstrap，Twitter 出品的知名前端设计库 [链接](http://v3.bootcss.com/)
* Materialize CSS，基于 Google Materialize 设计的前端库 [链接](http://materializecss.com/)
* AmazeUI，中国首个开源 HTML5 跨屏前端框架 [链接](http://amazeui.org/)

### 2.6.其他

* Underscore 类库源码(1.8.3) [链接](https://github.com/jashkenas/underscore)
* ws，包含前后端 websockets 实现的开发库，[链接](https://github.com/websockets/ws)

---

## 3.Web APIs

* Web API 参考-MDN，[链接](https://developer.mozilla.org/zh-CN/docs/Web/Reference/API)

*媒体技术*

* 媒体技术(Web Media Technologies)API-MDN，[链接](https://developer.mozilla.org/en-US/docs/Web/Media)

*通信技术*

* Websockets 数据通信 API-MDN，[链接](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)
* AJAX(XMLHttpRequest)API-MDN，[链接](https://developer.mozilla.org/en-US/docs/Web/API/XMLHttpRequest)
* Window.postMessage() API-MDN，[链接](https://developer.mozilla.org/zh-CN/docs/Web/API/Window/postMessage)
* 关于 JSONP(JSON with Padding)-如也，[链接](https://seanhuai.github.io/2017/10/seniverse-jsonp#关于-jsonp) 

---

## 4.NodeJS

* NodeJS-Download [链接](https://nodejs.org/en/download/)

### 4.1.核心

* NodeJS，基于 V8 引擎的 JavaScript 执行环境，官方网站 [链接](https://nodejs.org/)
* NodeJS 中文文档，[链接](http://nodejs.cn/api/)

### 4.2.包管理

* Yarn，新一代 NodeJS 包管理工具，[链接](https://yarn.bootcss.com/)  *建议使用Yarn替代NPM进行包管理工作*
* NPM，NodeJS 包管理器，[链接](https://www.npmjs.com/)
* NPM 使用介绍-菜鸟教程，[链接](http://www.runoob.com/nodejs/nodejs-npm.html)

```bash
npm --registry https://registry.npm.taobao.org info underscore 
```
因网络问题，可以使用以上代码更换NPM源

### 4.3.框架

* KOA，面向下一代的 NodeJS Web 服务框架，[链接](http://koajs.com/)
* Express，主流的 NodeJS Web 服务框架，[链接](http://www.expressjs.com.cn/)
* Sequelize，通用的数据库对象模型工具，[链接](http://docs.sequelizejs.com/)

---

## 5.Docker

**快速上手**
* Container 容器配置使用-如也 [链接](https://seanhuai.github.io/2017/05/container-config/)

**官方资源**
* Docker Docs [链接](https://docs.docker.com/)
* Get Docker [链接](https://www.docker.com/get-docker)  for CentOS [链接](https://docs.docker.com/engine/installation/linux/centos/)  for Ubuntu [链接](https://docs.docker.com/engine/installation/linux/ubuntu/) 

---

## 6.素材工具

**开发测试**

* mochajs，web 开发测试框架，[链接](http://mochajs.org/)
* ESLint，静态代码检查工具，[链接](https://eslint.org/)

**公共服务**

* BootCDN，免费的开源项目 CDN 库，[链接](http://www.bootcdn.cn/)
* Microsoft ASP.NET 开发资源 CDN 库，[链接](https://docs.microsoft.com/en-us/aspnet/ajax/cdn/overview)
* 有字库，网站云端字体 CDN 库，[链接](https://www.youziku.com/)

**通信测试**

* Fiddler，Windows 环境知名代理分析工具，[链接](https://www.telerik.com/fiddler)
* Charles，MacOS 环境知名代理分析工具，[链接](https://www.charlesproxy.com)
* Whistle，基于 Node 环境跨平台代理分析工具，[链接](https://github.com/avwo/whistle)
* Wireshark，跨平台数据包分析工具，[链接](https://www.wireshark.org)

**设计资源**

* Adobe Color CC，Adobe 在线配色设计站点，[链接](https://color.adobe.com/zh/explore/)
* Nippon Colors，日系设计配色站点，[链接](http://nipponcolors.com/)
* ICONFONT+，阿里巴巴矢量图标库，[链接](http://www.iconfont.cn)
* 方正字库，正版设计字体库，[链接](http://www.foundertype.com/)
* 百度像素，正版图片搜索引擎，[链接](http://pixel.baidu.com/)
* 瑞景创意，正版图片资源库，[链接](http://originoo.com/)

---

## 7.辅助资源

### 文档

* 编码规范 by @mdo [链接](http://codeguide.bootcss.com/)
* RESTful API 设计指南-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2014/05/restful_api.html)
* 中文技术文档的写作规范-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2016/10/document_style_guide.html)
* 开源许可证辨析-阮一峰的网络日志 [链接](http://www.ruanyifeng.com/blog/2011/05/how_to_choose_free_software_licenses.html)